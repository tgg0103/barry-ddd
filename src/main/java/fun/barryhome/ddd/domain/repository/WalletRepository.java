package fun.barryhome.ddd.domain.repository;

import fun.barryhome.ddd.domain.model.Wallet;

import java.util.List;

/**
 * Created on 2021/1/12 4:21 下午
 *
 * @author barry
 * Description:
 */
public interface WalletRepository {
    /**
     * 根据Id查询
     * @param walletId
     * @return
     */
    Wallet findById(String walletId);

    /**
     * 保存记录
     * @param wallet
     * @return
     */
    Wallet save(Wallet wallet);

    /**
     * 查询所有记录
     * @return
     */
    List<Wallet> findAll();
}
